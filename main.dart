import 'dart:convert';

import 'package:tojson_fromjson_learning/subject.dart';
import 'package:tojson_fromjson_learning/user.dart';
main() {

  var user1= User(
    "이영박",
    [
      Subject("개돼지","전공필수",0.3,4.5),
      Subject("미국인의 이해","교양노필요",0.2,4.5),
      Subject("컴퓨터공학","전공필수",0.1,4.5),
      Subject("컴퓨터똥학","전공필수",0.5,4.5),
    ],
  );
  String us = jsonEncode(user1);
  print(us);
}
class Subject{
  String? key;
  String name;
  String category;
  double point;
  double maxPoint;


  Subject(
    this.name,
    this.category,
    this.point,
    this.maxPoint
  );

  Subject fromJson(Map<String, dynamic> json){
    return Subject(
      this.name = json["과목이름"],
      this.category = json["과목분류"],
      this.point = json["학점"],
      this.maxPoint = json["최대학점"]
    );
  }

  toJson(){
    return {
      "과목이름" : this.name,
      "과목분류" : this.category,
      "학점" : this.point,
      "최대학점" : this.maxPoint
    };
  }
}
import 'package:tojson_fromjson_learning/subject.dart';

class User {
  String name;
  List<Subject> subjectList;

  User(
    this.name,
    this.subjectList
  );

  toJson(){
    return {
      "이름" : this.name,
      "과목리스트" : subjectList.map((e) => e.toJson()).toList(),
    };
  }
}

